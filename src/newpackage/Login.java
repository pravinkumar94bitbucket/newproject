package newpackage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Login {
	
	

	@FindBy(name = "email")
	private WebElement email;
	
	@FindBy(name = "password")
	private WebElement pass;
	
	@FindBy(id = "m_login_signin_submit")
	private WebElement signin;

	public void click() {
		signin.click();
	}
}
