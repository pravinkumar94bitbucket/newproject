package newpackage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MyClass2 {
	public static void main(String[] args) {
		System.setProperty("webdriver.gecko.driver", "/home/pravin/geckodriver");
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "http://prod.simultv.enveu.com";
		String tagName = "";
		String inputName = "input";

		driver.get(baseUrl);
		tagName = driver.findElement(By.id("email")).getTagName();
		System.out.println(tagName);
		if (tagName.contentEquals(inputName)) {
			System.out.println("Test Passed!");
		} else {
			System.out.println("Test Failed");
		}
		driver.close();
		System.exit(0);
	}
}