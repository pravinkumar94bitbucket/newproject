package newpackage;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MyClass {

	public static WebElement email;

	public static WebElement pass;

	public static WebElement signin;

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/home/pravin/chromedriver");
		WebDriver driver = new ChromeDriver();

		String baseUrl = "http://prod.simultv.enveu.com";
		String expectedTitle = "SimulTV CMS";
		String actualTitle = "";
		driver.get(baseUrl);
		actualTitle = driver.getTitle();
		if (actualTitle.contentEquals(expectedTitle)) {
			System.out.println(actualTitle + " : Test Passed!");
		} else {
			System.out.println(actualTitle + " : Test Failed");
		}

		driver.get("http://prod.simultv.enveu.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		email = driver.findElement(By.name("email"));
		pass = driver.findElement(By.name("password"));
		signin = driver.findElement(By.id("m_login_signin_submit"));
		email.sendKeys("pravinsingh@gmail.com");
		pass.sendKeys("veryeasy1");
		signin.click();
		Thread.sleep(3000);
		email.clear();
		pass.clear();

		email = driver.findElement(By.name("email"));
		pass = driver.findElement(By.name("password"));
		signin = driver.findElement(By.id("m_login_signin_submit"));
		email.sendKeys("");
		pass.sendKeys("");
		signin.click();
		Thread.sleep(3000);
		email.clear();
		pass.clear();

		email = driver.findElement(By.name("email"));
		pass = driver.findElement(By.name("password"));
		signin = driver.findElement(By.id("m_login_signin_submit"));
		email.sendKeys("simultvdemo@gmail.com");
		pass.sendKeys("veryeasy1");
		signin.click();
		Thread.sleep(3000);

		driver.findElement(By.linkText("Library Explorer")).click();
		driver.findElement(By.linkText("All Videos")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("Channel Explorer")).click();
		driver.findElement(By.linkText("All Channels")).click();
		Thread.sleep(3000);

		driver.findElement(By.linkText("Slots Management")).click();
		driver.findElement(By.linkText("Ad Slot Durations")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("Ad Slot Bookings")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("Pending For Approval")).click();
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/div/div/div[2]/div/ul/li[4]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("No")).click();
		Thread.sleep(3000);

		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/a/span[1]/img")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//*[@id='m_header_topbar']/div/ul/li/div/div/div[2]/div/ul/li[4]/a")).click();
		Thread.sleep(3000);
		driver.findElement(By.linkText("Yes")).click();
		driver.close();
		// driver.quit();
	}
}